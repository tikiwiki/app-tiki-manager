# app-tiki-manager

[![pipeline status](https://gitlab.com/tikiwiki/app-tiki-manager/badges/master/pipeline.svg)](https://gitlab.com/tikiwiki/app-tiki-manager/commits/master)

This repository allows to build RPM package of Tiki Manager App for ClearOS.

## Download latest RPM
[Download app-tiki-manager](https://gitlab.com/tikiwiki/app-tiki-manager/-/jobs/artifacts/master/download?job=rpm-clearos)

## Build RPM package
```
bash packaging/rpm-builder.sh --define "_sourcedir $PWD"
```
