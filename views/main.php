<?php

$this->lang->load('tiki_manager');
$this->load->library('session');

echo infobox_info(
    lang('webmanager_usage_with_link'),
    "<a target='_blank' href=". $app_url .">" . $app_url . "</a>"
);

echo form_open('tiki_manager/settings/edit', ['id' => 'tiki-manager-settings']);
echo form_header(lang('configuration_header'));

echo field_input('user_name', $user, lang('user'), true);
echo field_password('new_password', '', lang('new_password'), false);
echo field_password('new_password_confirmation', '', lang('repeat_new_password'), false);
echo field_button_set([form_submit_update(lang('update'))]);

echo form_footer();
echo form_close();