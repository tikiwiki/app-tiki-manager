<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'tiki_manager';
$app['version'] = '1.0.0';
$app['release'] = '1';
$app['vendor'] = 'Avantech'; // e.g. Acme Co
$app['packager'] = 'Avantech'; // e.g. Gordie Howe
$app['license'] = 'GPLv3'; // e.g. 'GPLv3';
$app['license_core'] = 'LGPLv3'; // e.g. 'LGPLv3';
$app['description'] = lang('tiki_manager_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('tiki_manager_app_name');
$app['category'] = lang('base_category_system');
$app['subcategory'] = 'Tiki'; // e.g. lang('base_subcategory_settings');
