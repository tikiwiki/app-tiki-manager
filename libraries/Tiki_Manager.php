<?php

namespace clearos\apps\tiki_manager;

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

clearos_load_language('tiki_manager');

use \clearos\apps\base\Daemon as Daemon;
use \clearos\apps\base\File as File;

clearos_load_library('base/Daemon');
clearos_load_library('base/File');

/**
 * Class Tiki_Manager
 * @description Class responsible for core operations related to Tiki-manager
 * @package clearos\apps\tiki_manager
 */
class Tiki_Manager extends Daemon
{
    const WEBMANAGER_CONFIG_FILE_PATH = "/opt/tiki-manager/webroot/config.php";
    const WEBMANAGER_RELATIVE_URL_PATH = "/tiki-manager";
    const WEBMANAGER_MIN_PASSWORD_LENGTH = 4;

    function __construct()
    {
        parent::__construct();
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Update the current webmanager's password
     * @param $new_password
     * @return bool
     */
    public function updateWebManagerPassword($new_password)
    {
        clearos_profile(__METHOD__, __LINE__);
        $old_password = $this->getWebManagerPassword();
        $file = new File(self::WEBMANAGER_CONFIG_FILE_PATH, true);
        $new_config_content = str_replace("define('PASSWORD', '$old_password');" , "define('PASSWORD', '$new_password');", $file->get_contents());

        try {
            // ClearOS documentation states this returns a boolean if changes were made,
            // But I've checked the replace_contents_locked core and it doesn't return any boolean at all
            // Check : https://gitlab.com/clearos/clearfoundation/app-base/blob/36043a54de0d429451ab2676a408d21741289d0c/libraries/File.php#L1583
            $file->replace_contents_locked($new_config_content);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    /**
     * Get the tiki-manager's current password
     * @return string
     */
    public function getWebManagerPassword()
    {
        clearos_profile(__METHOD__, __LINE__);
        $original_config_content = $this->getConfigContents();
        $str = explode("define('PASSWORD', '", $original_config_content)[1];
        $password = explode('\')', $str)[0];

        return $password;
    }

    /**
     * Get the configuration username
     * @return mixed
     */
    public function getUsername()
    {
        clearos_profile(__METHOD__, __LINE__);
        $original_config_content = $this->getConfigContents();
        $str = explode("define('USERNAME', '", $original_config_content)[1];
        $username = explode('\')', $str)[0];

        return $username;
    }

    /**
     * Generate the tiki-manager's webmanager application link
     * @return string
     */
    public function generateWebManagerLink()
    {
        clearos_profile(__METHOD__, __LINE__);
        $url = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443 ? "https://" : "http://";
        $url .= $_SERVER['SERVER_NAME'] . self::WEBMANAGER_RELATIVE_URL_PATH;

        return $url;
    }

    /**
     * Basic password validator
     * @param $password
     * @return string
     */
    public function passwordValidator($password)
    {
        if (strlen($password) < self::WEBMANAGER_MIN_PASSWORD_LENGTH) {
            return sprintf(lang('password_min_characters_required'), self::WEBMANAGER_MIN_PASSWORD_LENGTH);
        }
    }

    /**
     * Password confirmation validator
     * @param $password
     * @return mixed
     */
    public function passwordConfirmationValidator($password)
    {
        if ($_POST['new_password_confirmation'] !== $password) {
            return lang('password_confirmation_does_not_match');
        }
    }

    /**
     * Get tiki-manager configuration file contents
     * @return mixed
     */
    private function getConfigContents()
    {
        $file = new File(self::WEBMANAGER_CONFIG_FILE_PATH, TRUE);
        return $file->get_contents();
    }
}