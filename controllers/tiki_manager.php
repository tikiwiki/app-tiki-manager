<?php

/**
 * Class Tiki_Manager
 * Main controller
 */
class Tiki_Manager extends ClearOS_Controller
{
    /**
     * Method
     */
    function index()
    {
        $this->load->library('tiki_manager/Tiki_Manager');
        $this->lang->load('tiki_manager');

        $payload = [
            'app_url' => $this->tiki_manager->generateWebManagerLink(),
            'user' => $this->tiki_manager->getUsername(),
        ];

        $this->page->view_form('main', $payload, lang('tiki_manager_app_name'));
    }
}
