<?php

/**
 * Class Settings
 * Settings controller
 */
class Settings extends ClearOS_Controller
{
    /**
     * Function responsible for the basic logic related to the settings edit
     */
    function edit()
    {
        $this->load->library('tiki_manager/Tiki_Manager');
        $this->load->library('session');
        $this->load->library('form_validation');

        $this->form_validation->set_policy('new_password', 'tiki_manager/Tiki_manager', 'passwordValidator', true);
        $this->form_validation->set_policy('new_password_confirmation', 'tiki_manager/Tiki_manager', 'passwordConfirmationValidator', true);

        if (! $this->form_validation->run()) {
            $feedback_type = 'critical';
            $feedback_message = validation_errors();
            $feedback_title = lang('error');
        } else {
            $new_password = $this->input->post('new_password');
            $update_result = $this->tiki_manager->updateWebManagerPassword($new_password);

            $feedback_type = 'success';
            $feedback_message = lang('password_updated_successfully');
            $feedback_title =  lang('success');

            if ($update_result !== true) {
                $feedback_type = 'critical';
                $feedback_message = $update_result;
                $feedback_title = lang('error');
            }
        }

        $this->page->set_message($feedback_message, $feedback_type, $feedback_title);
        redirect('tiki_manager/index');
    }
}