Name:           app-tiki-manager
Version:        1.0
Release:        1%{?dist}
Summary:        Tiki-manager
License:        LGPLv3
BuildRequires:  rsync
Requires:       app-base-core
Requires:       app-tiki-manager-core

%description

%prep
ls -la %{_sourcedir}/
rsync -av --exclude=.git \
          --exclude=.gitignore \
          --exclude=.gitkeep \
          --exclude=README.md \
          --exclude=.gitlab-ci.yml \
          --exclude=packaging \
          %{_sourcedir}/ %{_builddir}/

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/tiki_manager
cp -r %{_builddir}/. %{buildroot}/usr/clearos/apps/tiki_manager/

%post
logger -p local6.notice -t installer 'app-tiki-manager - installing'

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-tiki-manager - uninstalling'
fi

%files
%attr(755,root,root)
/usr/clearos/apps/tiki_manager/
