#!/usr/bin/env bash

RPMBUILDER_FOLDER=/root/rpmbuild
CWD=$(dirname "$(realpath -s $0)")
SOURCEDIR=$(dirname "$CWD")

mkdir -p $RPMBUILDER_FOLDER/SPECS

cp $CWD/app-tiki-manager.spec $RPMBUILDER_FOLDER/SPECS/

cd $RPMBUILDER_FOLDER
rpmbuild -ba SPECS/app-tiki-manager.spec --define "_sourcedir $SOURCEDIR"

mkdir -p ${CWD}/rpms/
cp $RPMBUILDER_FOLDER/RPMS/x86_64/*.rpm ${CWD}/rpms/

