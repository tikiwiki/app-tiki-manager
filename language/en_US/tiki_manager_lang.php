<?php

$lang['tiki_manager_app_name'] = 'Tiki Manager';
$lang['tiki_manager_app_description'] = 'This application is a visual integration for tiki-manager in order to manage basic configurations.';

$lang['webmanager_usage_with_link'] = "Access Webmanager using the following link:";
$lang['old_password'] = "Old password";
$lang['new_password'] = "New password";
$lang['repeat_new_password'] = "Repeat new password";
$lang['configuration_header'] = "Configuration";
$lang['update'] = "Update";
$lang['success'] = "Success";
$lang['user'] = "User";
$lang['error'] = "Something went wrong";
$lang['password_updated_successfully'] = "Password updated successfully";
$lang['check_folder_permissions'] = "Password wasn't updated, please check the permissions";
$lang['password_min_characters_required'] = "Password requires a minimum of %s characters.</br>";
$lang['password_confirmation_does_not_match'] = "Password confirmation doesn't match with the provided password.</br>";
